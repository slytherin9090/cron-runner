# cron-runner

A simple cron runner that runs cron jobs based on json files. Auto loads everything from there.

## Purpose
- My fedora install's crond isnt working properly because of some SELinux shit, so instead of searching for a solution which I did already and havent got anything, wrote this dead simple cron runner that takes all json files stored in `cronjobs` folder, load and converts it to a cron job and runs based on the `schedule` field in the json file.

### TODO
- [ ] Make use of the existing express server to make an API that adds cronjobs
- [x] Add papertrail logging support
- [ ] Add mailer support in cronjobs
- [ ] Add SMS support
- [ ] Add build support ( for incremental builds )
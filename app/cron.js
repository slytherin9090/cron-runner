module.exports = function ({ id, name, description, schedule, command, isNode }) {
    /** create cron schedule based on data passed */
    var cron = require('node-cron');
    var shell = require('shelljs');
    var path = require('path');
    var createProgramLogger = require('../lib/logger').createProgramLogger;
    var logger = createProgramLogger(id);

    /** replaceVars replaces placeholder on command string */
    var replaceVars = (command) => {
        // replace scripts_folder
        const scriptFolder = path.join(__dirname, '../', 'scripts');

        return command.replace(/\{(.*)\}/igm, scriptFolder);
    }

    if (id, name, description, schedule, command) {
        
        if (cron.validate(schedule)) {
            return cron.schedule(schedule, () => {
                logger.info(`Running ${name} task.`, command);
                
                if(!isNode){
                    const ex = shell.exec(replaceVars(command), { silent: false });

                    if (ex.code !== 0) {
                        //failed
                        logger.info(`Task ${name} failed to run.`);
                        logger.info(ex.toString());
                    } else {
                        logger.info(`Completed ${name} task.`);
                        logger.info(ex.stdout);
                        logger.info(ex.stderr);
                    }
                } else {
                    const cronJob = require(replaceVars(command));
                    try {
                        cronJob(logger, () => {
                            logger.info(`Completed ${name} task.`);
                        });
                    } catch(ex){
                        //failed
                        logger.info(`Task ${name} failed to run.`);
                        logger.info(ex.toString());
                    }
                }
            });
        } else {
            logger.info(`Invalid cron schedule.`);
            return null;
        }


    } else {
        return null;
    }
}
var glob = require('glob');
var path = require('path');
var cronie = require('./cron');
var firebase = require('../lib/firebase');

/** Load cronjobs from files in ./cronjobs folder */

var cronjobsDir = path.join(__dirname, "../", "cronjobs");
var jobs = [];
glob(path.join(cronjobsDir, "**/*.cron.json"), (err, files) => {
    if(!err){
        files.forEach(file => {
            var cron = cronie(require(file));
            if(cron) {
                process.stdout.write(`Adding ${file} to cronjobs\n`);
                jobs.push(cron);
            }
        });
    }
});

module.exports = jobs;
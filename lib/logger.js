var winston = require('winston');
require('winston-papertrail').Papertrail;

let logger;

var winstonPapertrail = new winston.transports.Papertrail({
    host: 'logs5.papertrailapp.com',
    port: 26438,
    hostname: 'cron-runner',
    handleExceptions: false,
    colorize: true,
    logFormat: function (level, message) {
        return level + ': ' + message;
    }
});

winstonPapertrail.on('error', function (err) {
    console.error('PAPERTRAIL_ERROR', err);
});

logger = new winston.Logger({
    levels: {
        error: 0,
        warn: 1,
        auth: 2,
        debug: 3,
        info: 4
    },
    colors: {
        debug: 'blue',
        info: 'green',
        warn: 'yellow',
        error: 'red',
        auth: 'red'
    },
    transports: [winstonPapertrail]
});

logger.cron = (program, level, msg) => {
    let default_program = "default";

    if (!msg || msg === "") return;

    if (!!program) program = `app/${program.replace(/\s+/g, '').toLowerCase()}`;

    winstonPapertrail.program = !!program ? program : default_program;

    logger[level](`[${new Date().toLocaleString()}] ${msg}`);

    winstonPapertrail.program = default_program;
};

// this sets the base program
logger.createProgramLogger = function(baseProgram){
    let levels = ['error','warn','auth','debug','info'];
    let base = { };

    levels.forEach(level => {
        base[level] = function(msg){
            logger.cron(baseProgram, level, msg);
        }
    });

    return base;
}

if(process.env.NODE_ENV !== 'production'){
    logger.add(winston.transports.Console);
}



module.exports = logger;


module.exports = {
  apps : [{
    name: 'cron-jobs',
    script: 'index.js',
    instances: 1,
    autorestart: true,
    watch: true,
    ignore_watch : ["node_modules", "cron.log"],
    max_memory_restart: '1G',
    env : {
      NODE_ENV : 'production'
    }
  }]
};

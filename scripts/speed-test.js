module.exports = function(logger, onDone){
  var speedTest = require('speedtest-net');
  var firebase = require('../lib/firebase');
  var test = speedTest({maxTime: 5000});
  
  var db = firebase.database();
  var ref = db.ref('speed-tests');

  logger.info('Testing speed....');

  test.on('data', data => {
    data = Object.assign(data, { timestamp : new Date().toLocaleString() });
    ref.push(data);

    logger.info(`RESULT  ${data.speeds.download} DOWN,  ${data.speeds.upload} UP`);

    onDone();
  });
  
  test.on('error', err => {
    ref.push(err);
    logger.info('ERROR', err);
    onDone();
  });
}